.PHONY: all install uninstall

DESTDIR ?=
CONFDIR ?= /etc/initramfs-tools
UPDATE_INITRAMFS ?= update-initramfs

all:

install:
	install -m 0755 hook.sh $(DESTDIR)$(CONFDIR)/hooks/resizer
	install -m 0755 local-premount.sh $(DESTDIR)$(CONFDIR)/scripts/local-premount/resizer
	if ! test -e $(DESTDIR)/etc/default/resizer || cmp default.txt $(DESTDIR)/etc/default/resizer 2> /dev/null; then \
		install -m 0644 default.txt $(DESTDIR)/etc/default/resizer; \
	else \
		install -m 0644 default.txt $(DESTDIR)/etc/default/resizer.pkg; \
	fi
	$(UPDATE_INITRAMFS) -u

uninstall:
	rm -f $(DESTDIR)$(CONFDIR)/hooks/resizer
	rm -f $(DESTDIR)$(CONFDIR)/scripts/local-premount/resizer
	if test -e $(DESTDIR)/etc/default/resizer && cmp default.txt $(DESTDIR)/etc/default/resizer 2> /dev/null; then \
		rm -f $(DESTDIR)/etc/default/resizer; \
	fi
	rm -f $(DESTDIR)/etc/default/resizer.pkg
	$(UPDATE_INITRAMFS) -u
