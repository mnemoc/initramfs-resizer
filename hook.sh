#!/bin/sh

PREREQ="udev"
prereqs()
{
	echo "$PREREQ"
}

set -eu

case "${1:-}" in
prereqs)
	prereqs
	exit 0
	;;
esac

if [ -s /etc/default/resizer ]; then
	. /etc/default/resizer
fi

. /usr/share/initramfs-tools/hook-functions

# dependencies
#
# force reinstallation of `dd` because the existing version
# does support `bs=` and others
#
find "$DESTDIR" -name "dd" -exec rm -f {} \;

for x in dd readlink sgdisk lvm blkid partprobe resize2fs tune2fs; do
	f="$(which "$x")"
	if [ ! -x "$f" ]; then
		# missing
		echo "resizer: $x not found" >&2
		exit 1
	else
		# install
		copy_exec "$f"
	fi
done

# copy optional configuration
for x in resizer; do
	f="/etc/default/$x"
	if [ -s "$f" ]; then
		copy_file binary "$f"
	fi
done
