root partition resizer for initramfs-tools
==========================================

Helper module for debian/ubuntu's initramfs-tools to
allow shrinking the root partition before it gets mounted.

Known Limitations
-----------------

1. currently only supports ext4
2. root= needs to be in UUID mode

Usage
-----

Get the sources

```
git clone https://gitlab.com/mnemoc/initramfs-resizer
cd initramfs-resizer
```

if you want to use the default settings:
```
make install
reboot
```

and if you want to fiddle with the paramters first
```
make install UPDATE_INITRAMFS=true
vi /etc/default/resizer
update-initramfs -u
reboot
```

and after the reboot, uninstall it
```
cd initramfs-resizer
make uninstall
```

if you used LVM mode it's adviced to reinstall the bootloader

e.g.:
```
update-grub
grub-install /dev/sda
```

Parameters
----------

* `SPARE` represents the amount of free space we want in the root
  partition. Defaults to 10GiB.
* `USE_LVM` name of the LVM volume group which the shrunk root
  partition should be part of. Defaults to `no` (as in, "do not migrate to LVM").
* `EXIT_DELAY` integer seconds to wait at the end so you have time to look at the output.
  Defaults to 10.

LVM Booting
-----------

The first time you reboot in `USE_LVM` mode the bootloader will have used the old regular
partition, but the next time you'll need your bootloader to have been reconfigured for
lvm support.
