#!/bin/sh

set -eu

PREREQ=""

DEFAULT_SPARE=10GiB
DEFAULT_USE_LVM=no
DEFAULT_EXIT_DELAY=10
SPARE=
USE_LVM=
EXIT_DELAY=

prereqs()
{
	echo "$PREREQ"
}

case "${1:-}" in
	prereqs)
		prereqs
		exit 0
		;;
esac

if [ -s /etc/default/resizer ]; then
	. /etc/default/resizer
fi

# helpers
#
if [ -s /scripts/functions ]; then
	. /scripts/functions
	DEV_MODE=false
elif [ -s /usr/share/initramfs-tools/scripts/functions ]; then
	. /usr/share/initramfs-tools/scripts/functions
	DEV_MODE=true

	ROOT=
	quiet=n

	for x in $(cat /proc/cmdline); do
		case "$x" in
		root=*)
			export ROOT=${x#root=}
			;;
		esac
	done
else
	echo "resize: invalid runtime environment" >&2
	exit 1
fi

on_exit() {
	local action= x= t=${EXIT_DELAY:-$DEFAULT_EXIT_DELAY}

	if [ $# -gt 0 ]; then
		action="set +e"
		for x; do
			action="$action; $x"
		done
	fi

	if [ $t -gt 0 ]; then
		action="${action:+$action; }sleep $t"
	fi

	trap "$action" EXIT
}

run() {
	echo "+ $*" >&2

	if ! $DEV_MODE; then
		"$@" < /dev/null
	fi
}

rune() {
	echo "+ $*" >&2

	if $DEV_MODE; then
		sed "s/^/> /"
	else
		"$@"
	fi
}

die() {
	log_failure_msg "resizer: $*"
	exit 1
}

# cached tune2fs data
#
tune2fs_grep() {
	if [ $# -gt 0 ]; then
		echo "$tune2fs_raw" | grep "$@" | sed -e 's|^[^:]*:||' -e 's|^[ \t]*||' -e 's|[ \t]*$||'
	fi
}

tune2fs_reset() {
	tune2fs_raw=$(tune2fs -l "$1")
	[ -n "$tune2fs_raw" ]
}

# calculation helpers
#
div_up() {
	local a="$1" b="$2"
	a=$(($a + $b - 1))

	if [ "$b" = 0 ]; then
		echo 0
	else
		echo $(($a/$b))
	fi
}

trimsuffix() {
	local value="$1" x= v=
	shift

	for x; do
		v="${value%$x}"
		if [ "$v" != "$value" ]; then
			echo "$v"
			return
		fi
	done
}

param_convert_mb() {
	local value="${1:-0}" v=

	# GiB
	v=$(trimsuffix "$value" GiB G g)
	if [ -n "$v" ]; then
		echo $(($v * 1024))
		return
	fi

	# MiB
	v=$(trimsuffix "$value" MiB M m)
	if [ -n "$v" ]; then
		echo "$v"
		return
	fi

	# pass-through
	echo "$value"
}

#
# root scanner
#
ROOT_DEV=
ROOT_IS_UUID=

part_load_details() {
	local dev="${1:-$ROOT_DEV}" x=

	ROOT_IS_LVM=
	ROOT_PART_NAME=
	ROOT_PART_NUM=
	ROOT_PART_SYS=
	ROOT_PART_PATH=
	ROOT_PART_PATH_PREFIX=
	ROOT_PART_TYPE=
	ROOT_PART_LABEL=

	ROOT_PART_SECTOR_START=
	ROOT_PART_SECTOR_END=

	ROOT_PART_SIZE_SECTOR=
	ROOT_PART_SIZE_MB=

	DISC_NAME=
	DISC_PATH=

	if [ -n "$dev" -a -b "$dev" ]; then

		ROOT_PART_PATH="$dev"

		case "$dev" in
		/dev/*/*)
			ROOT_IS_LVM=true
			x=$(readlink -f "$dev" | sed -e 's|.*/||')

			DISC_NAME=$(echo "$dev" | cut -d/ -f2)
			ROOT_PART_NAME=$(echo "$dev" | cut -d/ -f3)
			ROOT_PART_SYS="/sys/class/block/$x"

			if [ "$DISC_NAME" = "mapper" ]; then
				DISC_NAME=${ROOT_PART_NAME%%-*}
				ROOT_PART_NAME=${ROOT_PART_NAME#*-}
			fi
			;;
		*)
			ROOT_IS_LVM=

			ROOT_PART_PATH_PREFIX=$(echo "$dev" | sed -e 's|[0-9]\+$||')
			ROOT_PART_NUM=${dev#$ROOT_PART_PATH_PREFIX}
			ROOT_PART_NAME=${ROOT_PART_PATH##*/}
			ROOT_PART_SYS="/sys/class/block/$ROOT_PART_NAME"

			DISC_PATH=${ROOT_PART_PATH_PREFIX%p}
			DISC_NAME=${DISC_PATH##*/}
			;;
		esac
	fi

	if [ -n "$ROOT_PART_SYS" -a -s "$ROOT_PART_SYS/size" ]; then
		# partition details
		#

		read ROOT_PART_SIZE_SECTOR < "$ROOT_PART_SYS/size"
		ROOT_PART_SIZE_MB=$(div_up $ROOT_PART_SIZE_SECTOR 2048)

		if [ -s "$ROOT_PART_SYS/start" ]; then
			read ROOT_PART_SECTOR_START < "$ROOT_PART_SYS/start"
			ROOT_PART_SECTOR_END=$(($ROOT_PART_SECTOR_START + $ROOT_PART_SIZE_SECTOR - 1))
		fi

		for x in $(blkid "$dev"); do
			case "$x" in
			TYPE=*)
				eval "ROOT_PART_TYPE=${x#TYPE=}"
				;;
			PARTLABEL=*)
				eval "ROOT_PART_LABEL=${x#PARTLABEL=}"
				;;
			esac
		done

		cat <<-EOT
		resizer: $dev ($DISC_NAME ${ROOT_PART_NUM:+$ROOT_PART_NUM }$ROOT_PART_NAME) ${ROOT_PART_TYPE:+$ROOT_PART_TYPE }$(div_up $ROOT_PART_SIZE_MB 1024) GiB
		EOT

		return 0 # success
	else
		return 1 # failure
	fi
}

fs_load_details() {
	local dev="$1" k=

	# filesystem details
	#
	FS_BLOCK_SIZE=
	FS_TOTAL_BLOCKS=
	FS_TOTAL_MB=
	FS_FREE_BLOCKS=
	FS_FREE_MB=
	FS_USED_BLOCKS=
	FS_USED_MB=

	if tune2fs_reset "$dev"; then

		FS_BLOCK_SIZE=$(tune2fs_grep -i "^block size")
		k=$((1024*1024/$FS_BLOCK_SIZE))

		FS_TOTAL_BLOCKS=$(tune2fs_grep -i "^block count")
		FS_TOTAL_MB=$(div_up $FS_TOTAL_BLOCKS $k)

		FS_FREE_BLOCKS=$(tune2fs_grep -i "^free blocks")
		FS_FREE_MB=$(div_up $FS_FREE_BLOCKS $k)

		FS_USED_BLOCKS=$(($FS_TOTAL_BLOCKS - $FS_FREE_BLOCKS))
		FS_USED_MB=$(div_up $FS_USED_BLOCKS $k)

		cat <<-EOT
		resizer: $ROOT_PART_TYPE: used:$FS_USED_MB free:$FS_FREE_MB total:$FS_TOTAL_MB
		EOT

		return 0 # success
	else
		return 1 # failure
	fi
}

expand_e2fs() {
	run resize2fs "$1"
	run e2fsck -y -f "$1"
}

shrink_e2fs() {
	run e2fsck -y -f "$1"
	run resize2fs -M "$1"
}

resize_part() {
	local disc="$1" part_num="$2" part_start="$3" part_end="$4" part_type="${5:-}" part_label="${6:-}"

	run sgdisk "$disc" \
		-d "$part_num" \
		-n "$part_num:$part_start:$part_end" \
		${part_type:+-t "$part_num:$part_type"} \
		${part_label:+-c "$part_num:$part_label"} \
		-g
}

resize_root_part() {
	local end="$1" type="${2:-8300}"

	resize_part "$DISC_PATH" "$ROOT_PART_NUM" \
		"$ROOT_PART_SECTOR_START" "$end" \
		$type "$ROOT_PART_LABEL"
}

#
on_exit

# check dependencies
#
for x in dd readlink sgdisk lvm blkid partprobe resize2fs tune2fs; do
	f=$(which "$x")
	if [ ! -x "$f" ]; then
		die "$x missing"
	fi
done

# find root device
#
case "${ROOT:-}" in
UUID=*)
	ROOT_DEV=$(blkid --uuid "${ROOT#UUID=}")
	ROOT_IS_UUID=true
	;;
*)
	die "only UUID= root is allowed"
	;;
esac

if ! part_load_details "$ROOT_DEV"; then
	die "failed to identify root=${ROOT:-}"
fi

if ! fs_load_details "$ROOT_PART_PATH"; then
	die "failed to get $ROOT_PART_TYPE details for $ROOT_PART_PATH"
fi

# determine objective
spare_mb=$(param_convert_mb ${SPARE:-$DEFAULT_SPARE})
goal_mb=$(($FS_USED_MB + $spare_mb))

MODE=
if ${ROOT_IS_LVM:-false}; then
	[ $FS_TOTAL_MB -le $goal_mb ] || MODE=lvm_shrink
elif [ "${USE_LVM:-$DEFAULT_USE_LVM}" = no ]; then
	[ $FS_TOTAL_MB -le $goal_mb ] || MODE=shrink
else
	MODE=migrate
fi

case "$MODE" in
lvm_shrink)
	# LV
	log_begin_msg "resize $(div_up $ROOT_PART_SIZE_MB 1024) -> $(div_up $goal_mb 1024) GiB LV"
	run lvm lvreduce -L ${goal_mb}m -vyr "$ROOT_PART_PATH"
	log_end_msg
	;;

shrink)
	# regular
	log_begin_msg "resize $(div_up $ROOT_PART_SIZE_MB 1024) -> $(div_up $goal_mb 1024) GiB"

	# shrink filesystem
	shrink_e2fs "$ROOT_PART_PATH"
	on_exit "expand_e2fs '$ROOT_PART_PATH'"
	fs_load_details "$ROOT_PART_PATH"

	# shrink partition
	resize_root_part +$(($FS_USED_MB + $spare_mb))M
	on_exit

	# expand filesystem to fit
	expand_e2fs "$ROOT_PART_PATH"

	log_end_msg
	;;

migrate)
	# regular to LVM

	log_begin_msg "migrate"

	# shrink partition to minimum
	#
	shrink_e2fs "$ROOT_PART_PATH"
	on_exit "expand_e2fs '$ROOT_PART_PATH'"
	fs_load_details "$ROOT_PART_PATH"

	resize_root_part +${FS_TOTAL_MB}M
	run e2fsck -y -f "$ROOT_PART_PATH"
	part_load_details "$ROOT_PART_PATH"

	# create clone partition
	#

	# find num for temporary partition
	last_num="$(sgdisk "$DISC_PATH" -p | grep '^ ' | tr -s ' ' | sort -n | tail -n 1 | cut -d' ' -f2)"
	clone_num=$(($last_num + 1))

	clone_sector_end=$(sgdisk "$DISC_PATH" -E)
	clone_sector_start=$(($clone_sector_end - $ROOT_PART_SIZE_SECTOR + 1))
	clone_path="$ROOT_PART_PATH_PREFIX$clone_num"

	run sgdisk "$DISC_PATH" -n "$clone_num:$clone_sector_start:$clone_sector_end" -t "$clone_num:8300" -c "$clone_num:clone" -g
	run dd if="$ROOT_PART_PATH" of="$clone_path" bs=4M
	run e2fsck -y -f "$clone_path"

	# prepare lvm partition
	#
	pv_path="$ROOT_PART_PATH"
	lv_path="/dev/mapper/${USE_LVM}-root"

	on_exit "sgdisk '$DISC_PATH' -t $ROOT_PART_NUM:8300 -g" \
		"expand_e2fs '$ROOT_PART_PATH'" \
		"sgdisk '$DISC_PATH' -d $clone_num"
	resize_root_part $(sgdisk "$DISC_PATH" -E) 8e00
	run lvm pvcreate -Zy -y "$pv_path"
	on_exit

	# create volume group
	run lvm vgcreate -Zy "$USE_LVM" "$pv_path"
	# create root lv
	run lvm lvcreate -Zy -Cy -ay -n "root" -L$(($ROOT_PART_SIZE_MB + $spare_mb)) "$USE_LVM"

	# restore data from clone partition
	run dd if="$clone_path" of="$lv_path" bs=4M
	run expand_e2fs "$lv_path"

	# delete clone
	run dd if=/dev/zero of="$clone_path" bs=1 count=1
	run sgdisk "$DISC_PATH" -d "$clone_num" -g

	# expand pv
	part_load_details "$pv_path"

	resize_root_part $(sgdisk "$DISC_PATH" -E) 8e00
	run partprobe "$DISC_PATH"
	run sgdisk "$DISC_PATH" -p
	run lvm pvresize "$pv_path"

	log_end_msg
	;;
esac
